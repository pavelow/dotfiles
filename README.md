# Dotfiles

[link](https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/)

`git clone --bare <git-repo-url> $HOME/.cfg`

`alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'`
