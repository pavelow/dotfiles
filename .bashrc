export TERM=xterm-color
alias l="ls -l --color"
alias ls='ls --color'
alias ll='ls -lah --color'
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
#exec fish

#Force the system to 'say' xterm

#export PS1=[\033[01m][ [\033[01;34m]\u@\h [\033[00m][\033[01m]] [\033[01;32m]\w[\033[00m]\n[\033[01;34m]$[\033[00m]]]]]]]]]>

export LESS='-R'
export LESSOPEN='|~/.lessfilter %s'

