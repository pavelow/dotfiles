set enc=utf-8
syntax on
set number relativenumber

call plug#begin('~/.vim/plugged')
Plug 'lervag/vimtex'
call plug#end()
